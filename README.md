# Based in the docker image featuring next 
# Docker - Mesa 3D OpenGL Software Rendering (Gallium) - LLVMpipe, and OpenSWR Drivers


## About

Minimal Docker container bundled with the Mesa 3D Gallium Drivers: [LLVMpipe][mesa-llvm] & [OpenSWR][openswr],  enabling OpenGL support inside a Docker container without the need for a GPU.
Minimal Docker with opengl-core 3.3 support without GPU (soft rendering) and forwarding connection to docker through docker host machine port forwarding

# Building the image from source dockerfile

```shell
docker build -f Dockerfile_idi -t opengl:idi sessions/
```
It needs XQuartz installed and network connection option enabled in security options
Commands in host for macOSX, it is similar in linux...

```shell
xhost + ## XQuartz net connections enabled
```

# HOWTO: Useful commands 

Start container from previously compiled image

```shell
docker run -d --name idi-opengl -p 3322:22 -v /tmp/.X11-unix:/tmp/.X11-unix \
-v $(pwd):/usr/local/bin -e TERM=xterm --entrypoint=/bin/sh -it opengl:idi
```

but it needs (TODO automatically) start sshd service

```shell
docker exec idi-opengl /etc/init.d/sshd start
```

For logging into the container

```shell 
docker exec -it idi-opengl /bin/bash
```

Or alternativelly, ssh in the docker container:

```shell 
ssh -XC -p 3322 <user_name>@<ip_docker_machine> 
```
Where <ip_docker_machine> is usually locahost or the docker ip in the virtual network (like 192.100.10...) it depends on docker version and host architecture so you need to search for
For the user_name and passwd, check inside the corresponding Dockerfile.  

For killing the container

```shell
docker rm -f idi-opengl
```

## Features

* Alpine Linux 3.7
* LLVMpipe Driver (Mesa 18.0.1)
* OpenSWR Driver (Mesa 18.0.1)
* OSMesa Interface (Mesa 18.0.1)
* softpipe - Reference Gallium software driver
* swrast - Legacy Mesa software rasterizer
* Xvfb - X Virtual Frame Buffer

## Docker Images

| Image                     | Description                                                             |
| ------------------------- | ----------------------------------------------------------------------- |
| `jamesbrink/opengl`       | Minimal image, good to extend `FROM`                                    |
| `jamesbrink/opengl:demos` | Same image with added mesa3d demos such as `glxinfo`, `glxgears`, etc.. |

## Usage (from original jamesbring container)

I build this image primarily to extnend from for other projects, but below are some simple examples. This image is already loaded with a trivial entrypoint script.  
Extending from this jamesbrink original image: 

```Dockerfile
FROM jamesbrink/opengl:18.0.1
COPY ./MyAppOpenGLApp /AnywhereMyHeartDesires
RUN apk add --update my-deps...
```

Running a simple glxgears test. 

```shell
docker run jamesbrink/opengl:demos glxgears -info
```

Running glxgears with OpenSWR

```shell
docker run -e GALLIUM_DRIVER=swr jamesbrink/opengl:demos glxgears -info
```

## Environment Variables

### High level settings

| Variable                | Default Value  | Description                                                    |
| ----------------------- | -------------- | -------------------------------------------------------------- |
| `XVFB_WHD`              | `1920x1080x24` | Xvfb demensions and bit depth.                                 |
| `DISPLAY`               | `:99`          | X Display number.                                              |
| `LIBGL_ALWAYS_SOFTWARE` | `1`            | Forces Mesa 3D to always use software rendering.               |
| `GALLIUM_DRIVER`        | `llvmpipe`     | Sets OpenGL Driver `llvmpipe`, `swr`, `softpipe`, and `swrast` |

### Lower level settings / tweaks

| Variable         | Default Value | Description                                                                                                                                                   |
| ---------------- | ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `LP_NO_RAST`     | `false`       | LLVMpipe - If set LLVMpipe will no-op rasterization                                                                                                           |
| `LP_DEBUG`       | `""`          | LLVMpipe - A comma-separated list of debug options is accepted                                                                                                |
| `LP_PERF`        | `""`          | LLVMpipe - A comma-separated list of options to selectively no-op various parts of the driver.                                                                |
| `LP_NUM_THREADS` | `""`          | LLVMpipe - An integer indicating how many threads to use for rendering. Zero (`0`) turns off threading completely. The default value is the number of CPU cores present. |






[openswr]: http://openswr.org/
[mesa-llvm]: https://www.mesa3d.org/llvmpipe.html



